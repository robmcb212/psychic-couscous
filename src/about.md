---
layout: default
title: This is me.
---
{{"
Hello! I'm on a journey through the exciting world of Google Cloud Platform (GCP).

 I've recently earned my Cloud Digital Leader certification and I'm currently gearing up for the Cloud Engineer Associate exam. This blog serves as my personal logbook, documenting the lessons I learn, the experiments I conduct, and the insights I gain along the way. 
 
 While this blog is primarily for my own growth and reflection, all the better if my experiences prove helpful to others navigating the vast landscape of GCP.  
 
 Rob. Software Professional. Edinburgh

"| newline_to_br}}