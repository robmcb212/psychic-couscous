---
title: "Real World Application on GCP"
date: "2024-08-15"
tags:
- Journal
- Spending Tracker
---

I haven't posted in a while as I have been consuming a lot of course content from Google Cloud Skills Boost, in particular their [Cloud Engineer Learning Path](https://www.cloudskillsboost.google/paths/11), in preparation for sitting my Associate Cloud Engineer exam. But there is only so much passive learning I can do before I need to mix in some actual development. 

I am therefore planning on producing a web application hosted fully on GCP to compliment my learning.
<!-- excerpt -->

## The Application

 The main purpose of creating and hosting a web application on GCP is to gain further practical experience using the serives and platform to help cement my learnings. The actual functionality of the application isn't particularly relevant, but it should have requirements in order to encourage the need for multiple GCP services. 

 I plan to build a very simple spending tracker web application. This will include authentication and CRUD (Create, Read, Update and Delete) operations of expendanture entries. 

 ## The Tech Stack

 For the front-end UI of the application I am going to choose [Angular](https://angular.dev/overview). At the time of writing this the lastest version of Angular is v18. My choice is based upon a curiosity into how Angular is as a framework given it's recent renaisance with the introduction of Signals, standalone components and the removal of the need of Zone.js. 

 I have written applications in past versions of Angular and enjoyed the experience, so this is a nice opportunity to revist it. The fact Angular is supported by Google also feels like a happy circumstance given the subject matter of this blog. 

 For the back-end, I plan to produce a REST API written in Python, using the [Django Rest Framework](https://www.django-rest-framework.org/). 

 ## GCP Hosting

 There are numerous hosting options within GCP, but for this project I plan to containerise both my Django backend and Angular front end using Docker and host them on [Cloud Run](https://cloud.google.com/run?hl=en).

Focussing on the Django backend API initially, this will include services such as: 
* [Cloud Run](https://cloud.google.com/run?hl=en) - managed compute platform
* [Cloud SQL](https://cloud.google.com/sql?hl=en) - managed PostgreSQL database
* [Cloud Storage](https://cloud.google.com/storage?hl=en) - unified object storage
* [Secret Manager](https://cloud.google.com/security/products/secret-manager) - manager sensitive secrets
* [Artifact Registry](https://cloud.google.com/artifact-registry) - container registry 
* [IAM](https://cloud.google.com/security/products/iam) - Service Account management
* [Terraform](https://www.terraform.io/) - Infrastructure as Code

## Next Steps 

In the coming months I plan to journal the actions I take along the way to make this happen. I will tag my related posts with 'Spending Tracker' as I build up the series. Wish me luck! 