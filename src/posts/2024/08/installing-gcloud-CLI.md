---
title: "Installing the gcloud CLI"
date: "2024-08-16"
tags:
- CLI
- Spending Tracker
---
There are four ways to interact with GCP: 
* the Google Cloud Platform Console (Web user interface)
* the Cloud Shell and Cloud SDK (Command-Line interface) 
* the Cloud Console Mobile App (For iOS and Android)
* REST-based APIs (For custom applications)

In this post, I will outline how to setup the Google Cloud SDK (Software Development Kit) onto your local machine. 
<!-- excerpt -->

## What is the Cloud SDK?

The Cloud SDK contains a set of command-line tools for the Google Cloud Platform. Most notably, it contains the `gcloud` and `kubectl` commands which you will utilise a lot as a GCP Cloud Engineer. It also contains the `gsutil` and `bq` utilities. 

The Cloud SDK contains client libraries for various programming languages too, but that is beyond the scope of this post. 

## What is the difference between the Cloud Shell and Cloud SDK? 

Sometimes it may not be convenient to install the Cloud SDK on the machine you are working with. Cloud Shell provides command-line access to your cloud resources directly from within your web browser.

Using Cloud Shell, you can manage your projects and resources easily without having to install the Cloud SDK or other tools locally. The Cloud SDK, gcloud, and kubectl command-line tools, and other utilities are always available up-to-date and fully authenticated.

How does it work? Cloud Shell is built using a Compute Engine virtual machine instance that you're not billed for and each GCP user has one automatically. 

Your Cloud Shell virtual machine is ephemeral, which means it'll be stopped whenever you stop using it interactively and it'll be restarted for you every time you re-enter Cloud Shell. Therefore, you wouldn't want to run a production web server in your Cloud Shell for example.

You'll also get five gigabytes of persistent disk storage that is reattached for you every time a new Cloud Shell session is started.

The Cloud Shell also includes a code editor is a tool for editing files inside of your Cloud Shell environment in real-time within your web browser.

Cloud Shell is extremely convenient and something I have used a lot during my learning. But in real world situations getting to grips with the `gcloud` CLI on your local machine is an essential skill. 

## Installing the Google Cloud SDK 

For the purposes of this post, I will be documenting the steps I took to install it on my MacBook with M1 Apple silicon. 

You can follow through the [official guide here](https://cloud.google.com/sdk/docs/install) for all major Operating Systems.

### Check your current Python version 

Via your terminal: `python3 --version`. Supported versions are 3.8 to 3.12. (I ran with Python 3.10.13)

### Download the Google Cloud SDK

From your terminal you can run `uname -m` to determine your machine hardware name. Use this knowledge to download the relevant package from the [Cloud SDK documentation](https://cloud.google.com/sdk/docs/install). 

Once downloaded extract the archive to your home directory (~). From your home directory in your terminal run the install script `./google-cloud-sdk/install.sh`. You will be asked if you want to opt-in to usage stats collection to help improve GCP, this is your choice. 

Presented to you is a list of all of the componenets that will be installed. 

{% asset_img '2024/08/GCP-CLI-install.png' 'CLI install components' %}

You will be asked if you want to 'Modify profile to update your $PATH and enable shell command completion?'. Answering 'Y' to this allows for the automatic update of your rc file meaning `gcloud` will be available in all future terminals you open. You should definately do this if it's your regular machine you will be using. 

You will now have to close your terminal window and open a new one for the changes to take effect.

It was at this stage that I was prompted to download and run Python 3.11 installer as Google Cloud CLI works best with this version at the time of running this. I opted to upgrade to Python 3.11, but I use [pyenv](https://realpython.com/intro-to-pyenv/) for managing my local Python versions, so I used this to upgrade to the desired version `arch -arm64 pyenv install 3.11.6` followed by `arch -arm64 pyenv global 3.11.6`.

Now to initialise gloud CLI run `./google-cloud-sdk/bin/gcloud init`. You will be prompted to sign into your Google account to proceed via your web browser.

And that's it, you are now officially running with the CLI! 


