---
title: "And so it begins"
date: "2024-06-04"
tags:
- Journal
---



I'm diving headfirst into the world of GCP, eager to explore its potential and unlock its power. As a complete beginner to GCP, I'm here to document my learning journey, share my experiences (both smooth and bumpy!), and hopefully provide some valuable insights for others starting their GCP adventure. 

So, whether you're a curious GCP newbie like me, or just looking to expand your GCP knowledge, join me on this exciting ride!

<!-- excerpt -->
