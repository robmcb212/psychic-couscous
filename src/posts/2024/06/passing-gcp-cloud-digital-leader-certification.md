---
title: "Passing the GCP Cloud Digital Leader certification"
date: "2024-06-14"
tags:
- Cloud Digital Leader
- Certification
---

In March 2024 I decided that I wanted to upskill in GCP. As well as practical experience, I wanted my learning to include theory. I know from experience that I personally benefit from following a structured learning path. I therefore concluded that I become GCP Certified was a great option for me personally. This lead me to investigate the certification structure of Google Cloud Platform, and ultimately to the ['GCP Cloud Digital Leader'](https://cloud.google.com/learn/certification/cloud-digital-leader) certification.
<!-- excerpt -->

The Cloud Digital Leader is Google's Foundation level certification. It's aim is to validate a broad knowledge of cloud concepts and the products, services and tools available in Google Cloud Platform. It doesn't require any technical prerequites. 

After searching the web for a number of resources (I personally have found value from [Adrien Cantrill's AWS courses](https://learn.cantrill.io/)) in the past. I struggled to find an equivalent resource for GCP. I therefore opted to use the free [Google Cloud Learning Path](https://www.cloudskillsboost.google/paths/9) as my main study resource. 

The learning path comprises of 6 activities with a suggested learning time of approximaltely 9 hours. All of the main content is video based, referencing documentation for further optional reading. I found production quality to be extremely high and well structured. For this foundation certification it's purely theoritical learning, so there wasn't any hands on labs required to compliment my learning. 

## My process 

In order to motivate myself I booked my exam 10 weeks in advance to ensure that I followed through with my consisent studies, and didn't get distracted. 

I watched all of the content, taking notes and screenshots of what I thought could be particularly relevant sections in the videos. I persoanlly use [bear](https://bear.app/) a markdown notes app on the apple ecosystem for my note taking. Due to me pausing the videos and documenting sections in my own words I think the entire content took around 14 hours to complete. 

In the final week run up to my exam and went through my notes, re-referencing multiple videos for further detials and reminders to the content I was struggling to remember. I also oped to hand right out even more summarised notes for each of the 6 modules on paper flashcards.

{% asset_img '2024/06/Cloud-digitial-leader-flashcards.jpeg' 'Cloud Digital Leader flashcards' %}

This process helped me commit a lot of the concepts to memory and have my flashcards to hand in the last few days run up whenever I wanted to swat up. 

## The exam

I opted to book my exam in a physical test centre, but there is the option to do this remotely. My decision came purely down to the fact I couldn't guarantee a quiet environment due to my young family at home. 

The test centre was in Edinburgh, Scotland. However, it was not a great experience. Gaining access to the test centre proved stressful as despite being there 30 mins before my exam was due, nobody was answering at main reception. Following that the centre had some technical issues getting the exam started, but I eventually got going 50 mins late than scheudled. Thankfully my time to complete the exam was not effected and the timer started when the digital exam commenced as opposed to my scheduled time. 

The exam is completely digital and complete at a computer terminal once you have safely stored your possessions in a locker. You are not allowed paper, phone or watch in the exam room.

I was able to comfortably complete all the questions, and was able to mark any questions that I might want to revisit at the end. Once I had completed every question there was ample time left to review my answers using an intuitive and simple interface. 

On submission, the terminal informed me I had passed provisionally pending a posible digital review and would hear back in a few days with confirmation. This was the case via the [Certmetrics](https://cp.certmetrics.com/) platform. 

Within 48 hours my pass was confirmed and I received my digital badge to confirm my certification pass via [Credly](https://www.credly.com/) which I proudly added to my Linked In profile. 

## Learnings

On reflection, I thinkg I over studied and could have passed with less maticulous memorization of low level details. However, I felt prepared and wasn't troubled by any of the questions. This is a learning I will take to my next goal, passing my [GCP Associate Cloud Engineer](https://cloud.google.com/learn/certification/cloud-engineer) which I am targetting to do by the end of 2024. 

